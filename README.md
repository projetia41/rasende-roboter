# Rasende Roboter

Artificial Intelligence project done during the UTBM's 2020 spring semester.
It's a little strategy game based on a simple concept : on the plate their is 4
robots and a target, the aim of the player is to move a specified robot to this
target in a minimum number of moves. The player play against an IA with multiple
difficulty levels.

# Install the project

To install the project, you first need to clone the repository. Then, go in the 
freshlycloned directory and create your virtual environement. You need to have 
python3,pygame and tkinter installed.

# Overview of the game

![Screenshot](overview.png)