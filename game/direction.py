from enum import Enum
import math


class Direction(Enum):
    NORTH = [0, -1]
    NORTH_EAST = [1, 1]
    EAST = [1, 0]
    SOUTH_EAST = [1, -1]
    SOUTH = [0, 1]
    SOUTH_WEST = [-1, -1]
    WEST = [-1, 0]
    NORTH_WEST = [-1, 1]


# Functions to get x and y offset of a direction
def get_x(direction):
    return direction.value[0]


def get_y(direction):
    return direction.value[1]


# Function that return the opposite direction of the given one
def get_opposite(direction):
    opposite = [element * -1 for element in direction.value]
    for dir in Direction:
        if opposite == dir.value:
            return dir


# Function that return the next direction clockwise: for example if we give NORTH as parameter we get EAST in return
def get_adjacent(direction):
    if direction.value[1] == -1 or direction.value[0] == -1:
        adjacent = [
            int(math.cos(math.acos(direction.value[0]) - (math.pi / 2))),
            int(math.sin(math.asin(direction.value[1]) - (math.pi / 2))),
        ]
    else:
        adjacent = [
            int(math.cos(math.acos(direction.value[0]) + (math.pi / 2))),
            int(math.sin(math.asin(direction.value[1]) + (math.pi / 2))),
        ]
    for dir in Direction:
        if adjacent == dir.value:
            return dir
