import pygame
import os
from tkinter import *
from tkinter import messagebox
import game
import time
import coord
import random
import gameObject
from math import *
import threading
import queue

# Initialization of pygame lib
pygame.init()
pygame.font.init()

# Placing the window on the screen
os.environ["SDL_VIDEO_WINDOW_POS"] = "0,0"

# Creating the window and setting it's tittle
window = pygame.display.set_mode((1200, 864))
pygame.display.set_caption("Rasende Roboter")

# Initializing the game
GAME = game.Game()
BOARD = GAME.board
MISSION_LEFT = 17
MISSION = None  # Actual mission
PAST_MISSION = []
OBJECTS = []
for obj in gameObject.GameObject:
    OBJECTS.append(obj)
random.shuffle(OBJECTS)
iaThread = None
queueThread = queue.Queue()
MOVES_NUMBER = 0
PLAYER_SCORE = 0
IA_SCORE = 0
STATUS = [
    "Reflechissez...",
    "A vous de jouer !",
    "L'adversaire joue...",
]  # Game status to print on the UI

menuButtonPos = []  # Hitboxes of menu's buttons
# Indicator for the board
hover = pygame.Surface((48, 48))
hover.fill((0, 0, 0,))
hover.set_alpha(128)
hover_rect = hover.get_rect(topleft=(48, 48))
choosed = pygame.Surface((48, 48))
choosed.fill((9, 89, 0))
choosed.set_alpha(100)
choosed_rect = choosed.get_rect()
to_go = []

select = False

# Clock
clock = pygame.time.Clock()
countdown = 0 * 1000

RUNNING = True  # Variable to check if the game is running or not
GAME_STATE = 0  # Variable to store the game state
STEP = 0  # Variable to store the different steps of a mission : 0 is for the reflexion step, 1 is for the player gaming turn, 2 is for the IA gaming turn
SELECTED = None  # Variable to store the selected bot to move
while RUNNING:
    # Event gestion
    for event in pygame.event.get():
        # If the player quit the window
        if event.type == pygame.QUIT:
            RUNNING = False
            pygame.quit()
            exit()
        elif event.type == pygame.MOUSEMOTION:
            pos = pygame.mouse.get_pos()
            if GAME_STATE == 1:
                if 48 <= pos[0] <= 816 and 48 <= pos[1] <= 816:
                    x = floor((pos[0] / 48) - 1)
                    y = floor((pos[1] / 48) - 1)
                    hover_rect.x = x * 48 + 48
                    hover_rect.y = y * 48 + 48
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            pos = pygame.mouse.get_pos()
            # If the player click on the Play button
            if menuButtonPos[0].collidepoint(pos) and GAME_STATE == 0:
                GAME_STATE = 1
            # If the player click on the Quit button
            elif menuButtonPos[1].collidepoint(pos) and GAME_STATE == 0:
                RUNNING = False  # We stop the main loop
                pygame.quit()  # We quit pygame
                exit()  # We exit the program properly
            # If the player click on the board's cases
            if 48 <= pos[0] <= 816 and 48 <= pos[1] <= 816:
                # If it's the player's moving turn
                if GAME_STATE == 1 and STEP == 1:
                    # We compute the board's coordinates depending on the clicked position on the screen
                    x = floor((pos[0] / 48) - 1)
                    y = floor((pos[1] / 48) - 1)
                    # If no robot is selected
                    if SELECTED == None:
                        # If the selected case has a robot, we select it
                        if BOARD.cases[x][y].has_bot():
                            SELECTED = BOARD.cases[x][y]
                            choosed_rect.x = 48 * x + 48
                            choosed_rect.y = 48 * y + 48
                            for d in BOARD.cases[x][y].destination:
                                if d.case.coord.x != x or d.case.coord.y != y:
                                    possible = pygame.Surface((48, 48))
                                    possible.fill((181, 11, 2))
                                    possible.set_alpha(128)
                                    possible_rect = possible.get_rect()
                                    possible_rect.x = d.case.coord.x * 48 + 48
                                    possible_rect.y = d.case.coord.y * 48 + 48
                                    to_go.append([possible, possible_rect])
                            select = True
                        else:
                            # We print an error message saying to the player to select a case with a robot first
                            Tk().wm_withdraw()
                            messagebox.showerror(
                                "Erreur",
                                "Vous devez d'abord sélectionné une case avec un robot !",
                            )
                    # Otherwise, if a robot is selected
                    else:
                        if x != SELECTED.coord.x or y != SELECTED.coord.y:
                            # We check every possible destination for the robot and we compare them to the selected coordinates
                            for d in SELECTED.destination:
                                # print(str(d.case.coord.x) + ";" + str(d.case.coord.y))
                                if d.case.coord.x == x and d.case.coord.y == y:
                                    if d.case.has_game_object():
                                        # If the correct robot is arriving on the mission case, we change the step to the ia's playing turn
                                        if (
                                            d.case.game_object == MISSION
                                            and BOARD.cases[SELECTED.coord.x][
                                                SELECTED.coord.y
                                            ].bot.color
                                            == MISSION.value[0]
                                        ):
                                            STEP = 2
                                            BOARD.move_bot(
                                                SELECTED.bot.color, d.case.coord
                                            )  # We move the robot
                                            BOARD.update()
                                            SELECTED = None
                                            select = False
                                            to_go = []
                                            MOVES_NUMBER += 1
                                            break
                                        # If not, we move still move it
                                        else:
                                            BOARD.move_bot(
                                                SELECTED.bot.color, d.case.coord
                                            )  # We move the robot
                                            BOARD.update()
                                            SELECTED = None
                                            select = False
                                            to_go = []
                                            MOVES_NUMBER += 1
                                            break
                                    # We just move the selected robot otherwise
                                    else:
                                        print("Moved")
                                        BOARD.move_bot(SELECTED.bot.color, d.case.coord)
                                        BOARD.update()
                                        SELECTED = None
                                        select = False
                                        to_go = []
                                        MOVES_NUMBER += 1
                                        break

                            # If no match is found, the robot can't move on the selected case so we print an error message
                            if SELECTED != None:
                                Tk().wm_withdraw()
                                messagebox.showerror(
                                    "Erreur",
                                    "La case choisi n'est pas une destination valide !",
                                )
                                SELECTED = None
                                to_go = []
                                select = False

    # Checking game state
    if GAME_STATE == 0:
        menuButtonPos = GAME.draw_menu(window)
    elif GAME_STATE == 1:
        # We set the framerate to 60fps
        clock.tick_busy_loop(60)
        countdown -= 1000 / 60

        # Choosing mission
        if MISSION == None and MISSION_LEFT > 0 and STEP == 0:
            for obj in OBJECTS:
                if obj not in PAST_MISSION:
                    MISSION = obj
                    PAST_MISSION.append(obj)
                    mission_text = "Recuperer le " + obj.value[0] + " " + obj.value[1]
                    iaThread = threading.Thread(
                        target=lambda q, arg1: q.put(GAME.ia_result(arg1)),
                        args=[queueThread, obj],
                    )
                    iaThread.start()
                    break
            MISSION_LEFT -= 1

        # IA playing turn, animating the found solution
        if STEP == 2:
            countdown = 0
            moves = []
            BOARD.reset_bot()
            print("Bonjour")
            GAME.draw_board(window)
            iaThread.join()
            moves = queueThread.get()
            for m in moves:
                BOARD.move_bot(m[0], m[1])
                GAME.draw_board(window)  # Updating the board after each move
                time.sleep(3)  # We wait 3 secs to slow the animation

            if MOVES_NUMBER > 0:
                if len(moves) < MOVES_NUMBER:
                    PLAYER_SCORE += 1
                elif len(moves) > MOVES_NUMBER:
                    IA_SCORE += 1
                else:
                    choosen = random.randint(1, 2)
                    if choosen == 1:
                        PLAYER_SCORE += 1
                    else:
                        IA_SCORE += 1
            else:
                IA_SCORE += 1
            MOVES_NUMBER = 0
            BOARD.remove_object(MISSION)
            MISSION = None
            STEP = 3

        # Checking state of the timer
        if countdown <= 0:
            if STEP == 0:
                STEP = 1  # Getting to the player gaming turn
                MOVES_NUMBER = 0  # Reseting player's number of moves
                countdown = 180 * 1000  # Setting the time of playing
            elif STEP == 1:
                STEP = 2
            elif STEP == 2:
                countdown = 0
            elif STEP == 3:
                STEP = 0
                countdown = 180 * 1000

        GAME.clear_screen(window)  # Clearing the window surface
        GAME.draw_board(window)  # Drawing the game board
        if STEP == 1:
            window.blit(hover, hover_rect)
            if select:
                window.blit(choosed, choosed_rect)
                for t in to_go:
                    window.blit(t[0], t[1])

        # Drawing the game ui
        GAME.draw_game_ui(
            window, countdown, PLAYER_SCORE, IA_SCORE, mission_text, STATUS[STEP]
        )

    # Updating window screen
    pygame.display.flip()
