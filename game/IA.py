import board
import case
import destination
import coord
import direction


class ia:
    def __init__(self, difficulty, board):
        self._difficulty = difficulty
        self._matrix = [[int] * 16 * 16 for i in range(16 * 16)]
        self._path = [[int] * 16 * 16 for i in range(16 * 16)]
        self._board = board
        self._object = []

        for i in range (16):
            for c in self._board.cases[i]:
                if c.has_bot():
                    if c.bot.color == "Red":
                        self._red = coord.Coord(c.coord.x,c.coord.y)
                        #print ("red ",self._red.x,self._red.y)
                    elif c.bot.color == "Green":
                        self._green = coord.Coord(c.coord.x,c.coord.y)
                        #print ("green ",self._green.x,self._green.y)
                    elif c.bot.color == "Blue":
                        self._blue = coord.Coord(c.coord.x,c.coord.y)
                        #print ("blue ",self._blue.x,self._blue.y)
                    elif c.bot.color == "Yellow":
                        self._yellow = coord.Coord(c.coord.x,c.coord.y)
                        #print ("yellow ",self._yellow.x,self._yellow.y)
                if c.has_game_object():
                    self._object.append([c.game_object,c.coord])

    def linkCaseColor (self,x,y,color):
        directions = [
            direction.Direction.NORTH,
            direction.Direction.EAST,
            direction.Direction.SOUTH,
            direction.Direction.WEST,
        ]
        can_go = []
        
        
        for dir in directions:
            actual_x = x
            actual_y = y
            # While the actual case has no wall in the direction we follow
            while not self._board.cases[actual_x][actual_y].has_walls_in_dir(dir):
                # If the next case has a bot, we stop
                if (self._board.cases[actual_x + direction.get_x(dir)][actual_y + direction.get_y(dir)].bot != None):
                    if self._board.cases[actual_x + direction.get_x(dir)][actual_y + direction.get_y(dir)].bot.color != color:
                        break
                # If their is no obstactle, we increase the test coordinates and we loop
                actual_x += direction.get_x(dir)
                actual_y += direction.get_y(dir)

            # When we exit the loop, we create a new destination with the last test coordinates
            can_go.append(destination.Destination(dir, self._board.cases[actual_x][actual_y]))

        return can_go

    def updateColor (self,color):
        for j in range(16):
                for i in range(16):
                    self._board.cases[i][j].reset_destinations()
                    if (j < 7 or j > 8) or (i < 7 or i > 8):
                        destinations = self.linkCaseColor(i, j,color)
                        for d in destinations:
                            self._board.cases[i][j].add_destination(d)

    def initMatrix(self,color):
        self.updateColor(color)
        for j in range(16 * 16):
            #if j == 16 * 7 + 7 or j == 16 * 7 + 8 or j == 16 * 8 + 7 or j == 16 * 8 + 8:
            #    continue
            for i in range(16 * 16):
               # if (i == 16 * 7 + 7 or i == 16 * 7 + 8 or i == 16 * 8 + 7 or i == 16 * 8 + 8):
                #    continue
                self._matrix[j][i] = 1000000
                for d in self._board.cases[j % 16][j // 16].destination:
                    if d.case.coord.x == (i % 16) and d.case.coord.y == (i // 16):
                        self._matrix[j][i] = 1
                if i == j:
                    self._matrix[i][i] = 0
                self._path[j][i] = j
        #print(self._matrix[0])

    def fillMatrix(self):
        for k in range(16 * 16):
            #if k == 16 * 7 + 7 or k == 16 * 7 + 8 or k == 16 * 8 + 7 or k == 16 * 8 + 8:
             #   continue
            for i in range(16 * 16):
              #  if (i == 16 * 7 + 7 or i == 16 * 7 + 8 or i == 16 * 8 + 7 or i == 16 * 8 + 8):
               #     continue
                for j in range(16 * 16):
                #    if (j == 16 * 7 + 7 or j == 16 * 7 + 8 or j == 16 * 8 + 7 or j == 16 * 8 + 8):
                 #       continue
                    if self._matrix[i][j] > self._matrix[i][k] + self._matrix[k][j]:
                        self._matrix[i][j] = self._matrix[i][k] + self._matrix[k][j]
                        self._path[i][j] = self._path[k][j]
        #print (self._matrix[0])

    def pr(self,path):
        for p in path:
            print (p[1].x,p[1].y)

    def returnPath(self,depart,arrivee,col):
        result=[]
        d = depart.x+depart.y*16
        a = arrivee.x+arrivee.y*16
        if self._matrix[d][a]==1000000:
            print ("impossible d'arriver à la case")
            result = None
        else:
            result.append([col,arrivee])
            for i in range (self._matrix[d][a]-1):
                a=self._path[d][a]
                result.append([col,coord.Coord(a%16,a//16)])
            result.reverse()
            self.pr(result)
        return result

    def easy(self,obj):

        for o in self._object:
            if o[0] == obj :
                if obj.value[0]=="Red":
                    self.initMatrix("Red")
                    self.fillMatrix()
                    return self.returnPath(self._red,o[1],"Red")
                elif obj.value[0]=="Blue":
                    self.initMatrix("Blue")
                    self.fillMatrix()
                    return self.returnPath(self._blue,o[1],"Blue")
                elif obj.value[0]=="Green":
                    self.initMatrix("Green")
                    self.fillMatrix()
                    return self.returnPath(self._green,o[1],"Green")
                elif obj.value[0]=="Yellow":
                    self.initMatrix("Yellow")
                    self.fillMatrix()
                    return self.returnPath(self._yellow,o[1],"Yellow")
    
        



        

    def returnResult(self,obj):
        if self._difficulty == 1:
            return self.easy(obj)
