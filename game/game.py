import pygame
import board
import case
import wall
import coord
import robot
import gameObject
import IA
import direction


class Game:
    def __init__(self):
        self._tileSize = 48
        self._colors = ["Blue", "Green", "Red", "Yellow"]
        self._board = board.Board()
        self._board.generate_board()
        self._ia = IA.ia(1, self._board)
        # list de tuple qui représentent les coups de l'ia sous la forme [couleur du robot, nouvelle position]
        self._ia_result = []

        # We load all the assets and background
        self._menuBackground = pygame.image.load(
            "./assets/img/MenuBackground.png"
        ).convert()
        self._objects = []
        for obj in gameObject.GameObject:
            img = pygame.image.load(
                "./assets/items/" + str(obj.value[0]) + str(obj.value[1]) + ".png"
            )
            self._objects.append([img, obj])
        self._robots = []
        for c in self._colors:
            self._robots.append(
                [
                    pygame.image.load(
                        "./assets/robots/" + str(c) + "Robot.png"
                    ).convert_alpha(),
                    c,
                ]
            )
        self._border = pygame.image.load("./assets/board/Border.png").convert_alpha()
        self._borderConnexions = [
            pygame.image.load("./assets/board/BorderConnexionLeft.png").convert_alpha(),
            pygame.image.load(
                "./assets/board/BorderConnexionRight.png"
            ).convert_alpha(),
        ]
        self._borderCorner = pygame.image.load(
            "./assets/board/BorderCorner.png"
        ).convert_alpha()
        self._case = pygame.image.load("./assets/board/Case.png").convert_alpha()
        self._center = pygame.image.load("./assets/board/Center.png").convert_alpha()
        self._wallExterior = [
            pygame.image.load("./assets/board/WallExteriorLeft.png").convert_alpha(),
            pygame.image.load("./assets/board/WallExteriorRight.png").convert_alpha(),
        ]
        self._wallInterior = [
            pygame.image.load("./assets/board/WallInteriorLeft.png").convert_alpha(),
            pygame.image.load("./assets/board/WallInteriorRight.png").convert_alpha(),
        ]
        self._wallCorner = [
            pygame.image.load("./assets/board/WallCornerInside.png").convert_alpha(),
            pygame.image.load("./assets/board/WallCornerOutside.png").convert_alpha(),
        ]
        # We load the font
        self._font = pygame.font.Font("./assets/font/SpaceQuestItalic.ttf", 20)
        self._fontXl = pygame.font.Font("./assets/font/SpaceQuestItalic.ttf", 42)

    # Method to draw the game menu
    def draw_menu(self, window):
        menuOptions = ["JOUER", "QUITTER"]
        menuButtons = [pygame.Surface((350, 100)), pygame.Surface((350, 100))]
        menuButtonPos = []
        count = 0
        for s in menuOptions:
            menuButtons[count].fill((156, 9, 26))

            contentText = self._fontXl.render(
                menuOptions[count], False, (255, 255, 255)
            )
            menuButtons[count].blit(
                contentText,
                (
                    (menuButtons[count].get_width() / 2)
                    - (contentText.get_width() / 2),
                    (menuButtons[count].get_height() / 2)
                    - (contentText.get_height() / 2),
                ),
            )

            menuButtonPos.append(
                self._menuBackground.blit(
                    menuButtons[count],
                    (
                        window.get_width() / 2 - menuButtons[count].get_width() / 2,
                        500 + 150 * count,
                    ),
                )
            )
            count += 1
        window.blit(self._menuBackground, (0, 0))
        return menuButtonPos

    # Method to draw the board
    def draw_board(self, window):
        board_cases = self._board.cases  # Getting the matrix of case
        board_surface = pygame.Surface((864, 864))  # Surface of the board
        start = 48
        angle_count = 0
        angle = [0, 90, 270, 180]

        # Firstly, we draw all the border walls and the corners
        board_surface.blit(pygame.transform.rotate(self._borderCorner, 0), (0, 0))
        for i in range(16):
            board_surface.blit(self._border, (start + i * 48, 0))
        board_surface.blit(pygame.transform.rotate(self._borderCorner, 180), (816, 816))
        for i in range(16):
            board_surface.blit(
                pygame.transform.rotate(self._border, 270), (816, start + i * 48)
            )
        board_surface.blit(pygame.transform.rotate(self._borderCorner, 90), (0, 816))
        for i in range(16):
            board_surface.blit(
                pygame.transform.rotate(self._border, -180), (start + i * 48, 816),
            )
        board_surface.blit(pygame.transform.rotate(self._borderCorner, 270), (816, 0))
        for i in range(16):
            board_surface.blit(
                pygame.transform.rotate(self._border, 90), (0, start + i * 48),
            )

        # Then we draw the case
        for i in range(16):
            for j in range(16):
                board_surface.blit(self._case, (start + i * 48, start + j * 48))

        # Then we iterate through the cases and we print the center, the object and the walls arround them and the bots
        for i in range(16):
            for j in range(16):
                if (i < 7 or i > 8) or (j < 7 or j > 8):
                    if board_cases[i][j].has_walls():
                        for w in board_cases[i][j].walls:
                            if (
                                j == 0 and i != 15
                            ) and w.dir == direction.Direction.EAST:
                                board_surface.blit(
                                    self.draw_walls("Border", "Up"), (start + i * 48, 0)
                                )
                            elif (
                                j == 15 and i != 15
                            ) and w.dir == direction.Direction.EAST:
                                board_surface.blit(
                                    self.draw_walls("Border", "Down"),
                                    (start + i * 48, 16 * 48),
                                )
                            elif (
                                i == 15 and j != 15
                            ) and w.dir == direction.Direction.SOUTH:
                                board_surface.blit(
                                    self.draw_walls("Border", "Right"),
                                    (16 * 48, start + j * 48),
                                )
                            elif (
                                i == 0 and j != 15
                            ) and w.dir == direction.Direction.SOUTH:
                                board_surface.blit(
                                    self.draw_walls("Border", "Left"),
                                    (0, start + j * 48),
                                )

                    if board_cases[i][j].has_game_object():
                        for o in self._objects:
                            if o[1] == board_cases[i][j].game_object:
                                board_surface.blit(
                                    o[0], (start + i * 48, start + j * 48)
                                )
                        if (i < 7 or i > 8) or (j < 7 or j > 8):
                            if (
                                board_cases[i][j]._walls[0].dir
                                == direction.Direction.NORTH
                                and board_cases[i][j]._walls[1].dir
                                == direction.Direction.WEST
                            ) or (
                                board_cases[i][j]._walls[1].dir
                                == direction.Direction.NORTH
                                and board_cases[i][j]._walls[0].dir
                                == direction.Direction.WEST
                            ):
                                board_surface.blit(
                                    self.draw_walls("Object", "UL"), (i * 48, j * 48),
                                )
                            elif (
                                board_cases[i][j]._walls[0].dir
                                == direction.Direction.NORTH
                                and board_cases[i][j]._walls[1].dir
                                == direction.Direction.EAST
                            ) or (
                                board_cases[i][j]._walls[1].dir
                                == direction.Direction.NORTH
                                and board_cases[i][j]._walls[0].dir
                                == direction.Direction.EAST
                            ):
                                board_surface.blit(
                                    self.draw_walls("Object", "UR"),
                                    ((i * 48) + start, j * 48),
                                )
                            elif (
                                board_cases[i][j]._walls[0].dir
                                == direction.Direction.SOUTH
                                and board_cases[i][j]._walls[1].dir
                                == direction.Direction.EAST
                            ) or (
                                board_cases[i][j]._walls[1].dir
                                == direction.Direction.SOUTH
                                and board_cases[i][j]._walls[0].dir
                                == direction.Direction.EAST
                            ):
                                board_surface.blit(
                                    self.draw_walls("Object", "DR"),
                                    (start + i * 48, start + j * 48),
                                )
                            elif (
                                board_cases[i][j]._walls[0].dir
                                == direction.Direction.SOUTH
                                and board_cases[i][j]._walls[1].dir
                                == direction.Direction.WEST
                            ) or (
                                board_cases[i][j]._walls[1].dir
                                == direction.Direction.SOUTH
                                and board_cases[i][j]._walls[0].dir
                                == direction.Direction.WEST
                            ):
                                board_surface.blit(
                                    self.draw_walls("Object", "DL"),
                                    (i * 48, start + j * 48),
                                )

                    if board_cases[i][j].has_bot():
                        for r in self._robots:
                            if r[1] == board_cases[i][j].bot.color:
                                board_surface.blit(
                                    r[0], (start + i * 48, start + j * 48)
                                )
                else:
                    board_surface.blit(
                        pygame.transform.rotate(self._center, angle[angle_count]),
                        (start + i * 48, start + j * 48),
                    )
                    angle_count += 1

        window.blit(board_surface, (0, 0))

    # Method to draw a surface containing a wall
    # Two types are possible : Border, for the walls sticked to the big border walls with 4 orientations (Up, Down, Right, Left)
    # And Object, for the walls around objects with 4 orientations: UL (Up Left), UR (Up Right), DL (Down Left), DR(Down Right)
    def draw_walls(self, type, orientation):
        wall_surface = pygame.Surface((96, 96))
        wall_surface.set_colorkey((0, 0, 0))
        if type == "Border":
            wall_surface.blit(self._borderConnexions[0], (0, 0))
            wall_surface.blit(self._borderConnexions[1], (48, 0))
            wall_surface.blit(self._wallExterior[0], (0, 48))
            wall_surface.blit(self._wallExterior[1], (48, 48))

            if orientation == "Up":
                return wall_surface
            if orientation == "Down":
                return pygame.transform.rotate(wall_surface, 180)
            elif orientation == "Right":
                return pygame.transform.rotate(wall_surface, 270)
            elif orientation == "Left":
                return pygame.transform.rotate(wall_surface, 90)
        elif type == "Object":
            wall_surface.blit(
                pygame.transform.rotate(self._wallInterior[0], 90), (48, 48)
            )
            wall_surface.blit(self._wallInterior[1], (48, 48))
            wall_surface.blit(self._wallExterior[0], (0, 48))
            wall_surface.blit(
                pygame.transform.rotate(self._wallExterior[1], 90), (48, 0)
            )
            wall_surface.blit(
                pygame.transform.rotate(self._wallCorner[0], 90), (48, 48)
            )
            wall_surface.blit(pygame.transform.rotate(self._wallCorner[1], 90), (0, 0))

            if orientation == "UL":
                return wall_surface
            elif orientation == "UR":
                return pygame.transform.rotate(wall_surface, -90)
            elif orientation == "DR":
                return pygame.transform.rotate(wall_surface, 180)
            elif orientation == "DL":
                return pygame.transform.rotate(wall_surface, 90)

    # Method to draw the game ui
    def draw_game_ui(self, window, time, player_score, ia_score, mission, status):
        start_x = 18 * 48
        ui_width = window.get_width() - start_x
        size = ui_width, window.get_height()
        WHITE = (255, 255, 255)

        # Ui background
        ui_background = pygame.Surface(size)
        ui_background.fill((135, 129, 128))

        # Time sprite
        hours, remainder = divmod(time / 1000, 3600)
        minutes, seconds = divmod(remainder, 60)

        time_text = self._fontXl.render(
            str("{:02}:{:02}".format(int(minutes), int(seconds))), False, WHITE
        )
        ui_background.blit(
            time_text, ((ui_width / 2) - (time_text.get_width() / 2), 50)
        )

        # Status
        status_text = self._font.render(status, False, WHITE)
        ui_background.blit(
            status_text, ((ui_width / 2) - (status_text.get_width() / 2), 150)
        )

        # Objectif
        goal_title_text = self._font.render("Mission:", False, WHITE)
        ui_background.blit(
            goal_title_text, ((ui_width / 2) - (goal_title_text.get_width() / 2), 250)
        )
        goal_text = self._font.render(mission, False, WHITE)
        ui_background.blit(
            goal_text, ((ui_width / 2) - (goal_text.get_width() / 2), 275)
        )

        # Score
        score_title_text = self._font.render("Score:", False, WHITE)
        ui_background.blit(
            score_title_text, ((ui_width / 2) - (score_title_text.get_width() / 2), 380)
        )

        # Player
        player_score_text = self._font.render(
            "Player : " + str(player_score), False, WHITE
        )
        ui_background.blit(
            player_score_text,
            ((ui_width / 2) - (player_score_text.get_width() / 2), 430),
        )

        # IA
        ia_score_text = self._font.render("IA : " + str(ia_score), False, WHITE)
        ui_background.blit(
            ia_score_text, ((ui_width / 2) - (ia_score_text.get_width() / 2), 460)
        )

        # Final blit
        window.blit(ui_background, (start_x, 0))

    # Method to clear the screen
    def clear_screen(self, window):
        cleaned = pygame.Surface(window.get_size())
        cleaned.fill
        cleaned.set_alpha(255)
        window.blit(cleaned, (0, 0))

    @property
    def board(self):
        return self._board

    def ia_result(self, mission):
        self._ia_result = self._ia.returnResult(mission)
        return self._ia_result
