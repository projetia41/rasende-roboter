import board
import case
import destination
import coord
import threadFloyd
import threading

class floyd:
    def __init__ (self,difficulty,board):
        self._threadLock = threading.Lock()
        self._difficulty = difficulty
        self._matrix = [[int]* 16*16 for i in range(16*16)]
        self._path = [[int]* 16*16 for i in range(16*16)]
        self._board = board

    def initMatrix (self):
        threads = []
        for i in range (16*16):
            if (not(i==16*7+7 or i==16*7+8 or i==16*8+7 or i==16*8+8)):
                thread = threadFloyd.threadFloyd(i,self._matrix,self._path,self._board,"Init",self._threadLock)
                thread.start()
                print("thread started")
                threads.append(thread)
        for t in threads:
            t.join()
            print("thread stoped")
        print(self._matrix[0])

    def fillMatrix (self):
        threads = []
        for k in range (16*16):
            if (k==16*7+7 or k==16*7+8 or k==16*8+7 or k==16*8+8):
                thread = threadFloyd.threadFloyd(k,self._matrix,self._path,self._board,"SOlve",self._threadLock)
                thread.start()
                print("thread started")
                threads.append(thread)
        for t in threads:
            t.join()
            print("thread stoped")
