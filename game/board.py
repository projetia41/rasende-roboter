import case
import wall
import coord
import direction
import gameObject
import robot
import destination

import random
from datetime import datetime


class Board:
    def __init__(self):
        # Init the board
        self._cases = [[case.Case(coord.Coord(0, 0))] * 16 for i in range(16)]
        # We set correctly all the coordinates for each case of the board
        for j in range(16):
            for i in range(16):
                if (j < 7 or j > 8) or (i < 7 or i > 8):
                    self._cases[i][j] = case.Case(coord.Coord(i, j))

    def generate_board(self):
        # We add the border walls and the center walls
        for j in range(16):
            self._cases[j][0].add_wall(wall.Wall(direction.Direction.NORTH))
            self._cases[0][j].add_wall(wall.Wall(direction.Direction.WEST))
            self._cases[j][15].add_wall(wall.Wall(direction.Direction.SOUTH))
            self._cases[15][j].add_wall(wall.Wall(direction.Direction.EAST))

            for i in range(16):
                if i == 7 or i == 8:
                    if j == 6:
                        self._cases[i][6].add_wall(wall.Wall(direction.Direction.SOUTH))
                    elif j == 9:
                        self._cases[i][9].add_wall(wall.Wall(direction.Direction.NORTH))
                elif j == 7 or j == 8:
                    if i == 6:
                        self._cases[6][j].add_wall(wall.Wall(direction.Direction.EAST))
                    elif i == 9:
                        self._cases[9][j].add_wall(wall.Wall(direction.Direction.WEST))

        # We add the walls sticked to the border walls
        big_walls_pos = [
            [direction.Direction.EAST, "j", 0],
            [direction.Direction.SOUTH, "i", 15],
            [direction.Direction.EAST, "j", 15],
            [direction.Direction.SOUTH, "i", 0],
        ]
        for pos in big_walls_pos:
            for i in range(2):
                index = self.choose_wall_index(i)
                if pos[1] == "j":
                    self._cases[index][pos[2]].add_wall(wall.Wall(pos[0]))
                    self._cases[index + 1][pos[2]].add_wall(
                        wall.Wall(direction.get_opposite(pos[0]))
                    )
                elif pos[1] == "i":
                    self._cases[pos[2]][index].add_wall(wall.Wall(pos[0]))
                    self._cases[pos[2]][index + 1].add_wall(
                        wall.Wall(direction.get_opposite(pos[0]))
                    )

        # We place randomly the objects on the board
        # First we put all the objects in a list
        objects = []
        placed_objects_coo = []
        for obj in gameObject.GameObject:
            objects.append(obj)

        # Then we place them, by checking if their is no object nearby
        for obj in objects:

            while "Les coordonnées ne sont pas générées ou pas acceptable":
                x_coord = random.randint(2, 13)
                y_coord = random.randint(2, 14)
                pos = [x_coord, y_coord]
                if pos not in placed_objects_coo:
                    if (x_coord < 7 or x_coord > 8) or (y_coord < 7 or y_coord > 8):
                        if self.is_nearby(x_coord, y_coord) == False:
                            placed_objects_coo.append(pos)
                            self._cases[x_coord][y_coord].add_game_object(obj)
                            break
                        else:
                            continue

        # We generate the walls around the objects
        directions = [
            direction.Direction.NORTH,
            direction.Direction.EAST,
            direction.Direction.SOUTH,
            direction.Direction.WEST,
        ]
        for p in placed_objects_coo:
            random.shuffle(directions)
            direction_index = random.randint(0, 3)
            dir = directions[direction_index]
            # We add a wall in the chosen direction
            self._cases[p[0]][p[1]].add_wall(wall.Wall(dir))
            # We add a wall in the opposite direction of the chosen one in the case just after the actual case in the chosen direction
            self._cases[p[0] + direction.get_x(dir)][
                p[1] + direction.get_y(dir)
            ].add_wall(wall.Wall(direction.get_opposite(dir)))
            # We add to the actual case a wall with tha adjacent direction of the chosen one
            self._cases[p[0]][p[1]].add_wall(
                wall.Wall(direction.get_adjacent(directions[direction_index]))
            )
            # We add a wall in the oppisute direction of the adjacent direction of the chosen one in the case juste after the actual case in the adjacent direction
            # of the chosen one
            self._cases[p[0] + direction.get_x(direction.get_adjacent(dir))][
                p[1] + direction.get_y(direction.get_adjacent(dir))
            ].add_wall(wall.Wall(direction.get_opposite(direction.get_adjacent(dir))))

        

        # And finally we place randomly the robots on the board
        bot_placed_coo = []
        colors = ["Blue", "Green", "Red", "Yellow"]
        for i in range(2):
            for j in range(2):
                random.shuffle(colors)
                color = colors[random.randint(0, len(colors) - 1)]
                colors.remove(color)

                while "Les coordonnées ne sont pas générées ou pas acceptable":
                    x_coord = random.randint(2, 7) + j * 7
                    y_coord = random.randint(2, 7) + i * 7

                    pos = [x_coord, y_coord]
                    if pos not in bot_placed_coo:
                        if (x_coord < 6 or x_coord > 9) or (y_coord < 6 or y_coord > 9):
                            if self._cases[x_coord][y_coord].has_game_object() == False:
                                bot_placed_coo.append(pos)
                                self._cases[x_coord][y_coord].place_bot(
                                    robot.Robot(color, coord.Coord(x_coord, y_coord))
                                )
                                break
        
        # Then, we store in each case of the plate, the case accesible in 1 move
        for j in range(16):
            for i in range(16):
                if (j < 7 or j > 8) or (i < 7 or i > 8):
                    destinations = self.link_case(i, j)
                    for d in destinations:
                        self._cases[i][j].add_destination(d)

    # Methods to compute for each case the cases on which we can go in one move
    def link_case(self, x, y):
        directions = [
            direction.Direction.NORTH,
            direction.Direction.EAST,
            direction.Direction.SOUTH,
            direction.Direction.WEST,
        ]
        can_go = []

        for dir in directions:
            actual_x = x
            actual_y = y
            # While the actual case has no wall in the direction we follow
            while not self._cases[actual_x][actual_y].has_walls_in_dir(dir):
                # If the next case has a bot, we stop
                if (
                    self._cases[actual_x + direction.get_x(dir)][
                        actual_y + direction.get_y(dir)
                    ].bot
                    != None
                ):
                    break
                # If their is no obstactle, we increase the test coordinates and we loop
                actual_x += direction.get_x(dir)
                actual_y += direction.get_y(dir)

            # When we exit the loop, we create a new destination with the last test coordinates
            can_go.append(destination.Destination(dir, self._cases[actual_x][actual_y]))

        return can_go

    # Simple method to choose randomly an index to place a wall
    def choose_wall_index(self, part):
        rnd = random.randint(2, 6)
        return rnd + (part * 8)

    # Method that check in a radius of one arround de given coordinates if their is an other object
    def is_nearby(self, x, y):
        y_test = y - 1
        for i in range(3):
            x_test = x - 1
            for j in range(3):
                if (
                    self._cases[x_test][y_test].has_game_object()
                    or x_test == 7
                    or x_test == 8
                    or y_test == 7
                    or y_test == 8
                ):
                    return True
                x_test += 1
            y_test += 1
        return False

    # Method to move a bot by giving it color and the new coord
    def move_bot(self, color, coord):
        for i in range(16):
            for j in range(16):
                if self._cases[i][j].has_bot():
                    if self._cases[i][j].bot.color == color:
                        robot = self._cases[i][j].bot
                        self._cases[i][j].bot.move(coord)
                        self._cases[coord.x][coord.y].place_bot(robot)
                        self._cases[i][j].remove_bot()
                        break
            else:
                continue
            break

    # Method to remove an object of the board at the end of the round
    def remove_object(self, object):
        for i in range(16):
            for j in range(16):
                if self._cases[i][j].has_game_object():
                    if self._cases[i][j].game_object == object:
                        self._cases[i][j].remove_game_object()

    # Method to replace the moved bots to their start position
    def reset_bot(self):
        for i in range(16):
            for j in range(16):
                if self._cases[i][j].has_bot():
                    robot = self._cases[i][j].bot
                    if (
                        robot.pos.x != robot.startPos.x
                        or robot.pos.y != robot.startPos.y
                    ):
                        robot.reset_pos()
                        self._cases[robot.startPos.x][robot.startPos.y].place_bot(robot)
                        self._cases[i][j].remove_bot()

    # Method to update board model, to re-compute the destination possible for each case after a robot moves
    def update(self):
        for j in range(16):
            for i in range(16):
                self._cases[i][j].reset_destinations()
                if (j < 7 or j > 8) or (i < 7 or i > 8):
                    destinations = self.link_case(i, j)
                    for d in destinations:
                        self._cases[i][j].add_destination(d)

    @property
    def cases(self):
        return self._cases
