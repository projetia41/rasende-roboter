import threading
import board
import time

class threadFloyd(threading.Thread):
    def __init__ (self,caseId,matrix,path,board,mode,threadLock):
        threading.Thread.__init__(self)
        self._caseId = caseId
        self._matrix = matrix
        self._board = board
        self._path = path
        self._mode = mode
        self._threadLock = threadLock

    def run(self):
        self._threadLock.acquire()
        x = self._caseId%16
        y = self._caseId//16
        if(self._mode == "Init"):
            self._matrix[x][y] = 1000000
            for d in self._board.cases[x][y].destination:
                if (d.case.coord.x == x and d.case.coord.y == y):
                    self._matrix[x][y] = 1
            if(x == y):
                self._matrix[x][y]= 0
            self._path[x][y] = y
        if(self._mode == "Solve"):
            for i in range (16*16):
                if (i==16*7+7 or i==16*7+8 or i==16*8+7 or i==16*8+8):
                    continue
                for j in range (16*16):
                    if (j==16*7+7 or j==16*7+8 or j==16*8+7 or j==16*8+8):
                        continue                
                    if (self._matrix[i][j] > self._matrix[i][caseId]+self._matrix[caseId][j]):
                        self._matrix[i][j] = self._matrix[i][caseId]+self._matrix[caseId][j]
                        self._path[i][j] = self._path[caseId][j]
        self._threadLock.release()
    
        