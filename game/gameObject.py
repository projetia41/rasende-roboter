from enum import Enum


class GameObject(Enum):
    VORTEX = ["", "Vortex"]
    BLUE_BALL = ["Blue", "Ball"]
    RED_BALL = ["Red", "Ball"]
    GREEN_BALL = ["Green", "Ball"]
    YELLOW_BALL = ["Yellow", "Ball"]
    BLUE_COIN = ["Blue", "Coin"]
    RED_COIN = ["Red", "Coin"]
    GREEN_COIN = ["Green", "Coin"]
    YELLOW_COIN = ["Yellow", "Coin"]
    BLUE_RING = ["Blue", "Ring"]
    RED_RING = ["Red", "Ring"]
    GREEN_RING = ["Green", "Ring"]
    YELLOW_RING = ["Yellow", "Ring"]
    BLUE_BEACON = ["Blue", "Beacon"]
    RED_BEACON = ["Red", "Beacon"]
    GREEN_BEACON = ["Green", "Beacon"]
    YELLOW_BEACON = ["Yellow", "Beacon"]
